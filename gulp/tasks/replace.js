var conf = require('../config.json');
var gulp = require('gulp');
var replace = require('gulp-replace');

gulp.task('replace', function(){
  gulp.src(conf.base.build + '*.html')
    .pipe(replace('/vendor', '../vendor'))
    .pipe(gulp.dest(conf.base.build));
});