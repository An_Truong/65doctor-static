var conf = require('../config.json');
var pkg = require('../../package.json');
var gulp = require('gulp');
var htmlhint = require('gulp-htmlhint');
var jade = require('gulp-jade');
var inject = require('gulp-inject');
var replace = require('gulp-replace');
var usemin = require('gulp-usemin');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var rev = require('gulp-rev');
 
gulp.task('pug:build', function() {
  var my_locals = {
    "appName": pkg.name
  };
 
  gulp.src([conf.base.src + conf.files.pug])
    .pipe(jade({
      locals: my_locals,
      pretty: true
    }))
    .pipe(inject(gulp.src(conf.vendor.jsCss, {read: false}, {relative: true})))
    .pipe(gulp.dest(conf.base.build))
    .pipe(htmlhint.reporter());
});

gulp.task('pug:compile', function() {
  var my_locals = {
    "appName": pkg.name
  };
 
  gulp.src([conf.base.src + conf.files.pug])
    .pipe(jade({
      locals: my_locals,
      pretty: true
    }))
    .pipe(inject(gulp.src(conf.vendor.jsCss, {read: false}, {relative: true})))
    .pipe(replace('/vendor', '../vendor'))
    .pipe(gulp.dest(conf.base.build))
    .pipe(htmlhint.reporter());
});