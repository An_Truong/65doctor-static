'use strict';
var app = (function ($, window, undefined) {

    var utils = (function () {
        return {
            escapeRegExChars: function (value) {
                return value.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&');
            },
            createNode: function (containerClass) {
                var div = document.createElement('div');
                div.className = containerClass;
                div.style.position = 'absolute';
                div.style.display = 'none';
                return div;
            }
        };
    }());

    function toogleDoctorInfoHandle() {
        $('.toogle-info').click(function(e) {
            e.preventDefault();
            var _this = $(this);

            _this.toggleClass('fa-angle-down');
            _this.toggleClass('fa-angle-up');

            _this.parents('.doctor-group').find('.doctor-group__toogle').stop().slideToggle();
        });
    }

    function init() {
    	$('.carousel').slick({
    		slidesToShow: 3,
  			slidesToScroll: 1
    	});

        $('.eshop-banner').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            arrows: false
        });

        $('.article-list').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            dots: true,
            arrows: false
        });

        $('.room-list').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false
        });

        $('.doctor__more').click(function(e) {
            e.preventDefault();
            var _this = $(this),
                _icon = _this.find('i');

            _icon.toggleClass('fa-arrow-circle-down');
            _icon.toggleClass('fa-arrow-circle-up');

            _this.parents('.result__item').find('.doctor__info').stop().slideToggle();
        });

        if($('.result__head').length) {
            $('.result__head').scrollToFixed({
                limit: $('.pagination-wrap').offset().top - 100
            });
        }

        $('ul.dropdown-menu').on('click', function(e){
            var events = $._data(document, 'events') || {};
            events = events.click || [];
            for(var i = 0; i < events.length; i++) {
                if(events[i].selector) {
                    if($(e.target).is(events[i].selector)) {
                        events[i].handler.call(e.target, e);
                    }
                    $(e.target).parents(events[i].selector).each(function(){
                        events[i].handler.call(this, e);
                    });
                }
            }
            e.stopPropagation();
        });

        $('.search__form__input').devbridgeAutocomplete({
            serviceUrl: 'mocks/doctors.json',
            dataType: 'json',
            formatResult: function(suggestion, currentValue) {
                var key = '<span class="suggestion-key">'+ suggestion.data +'</span>';
                if (!currentValue) {
                    return suggestion.value + key;
                }
                
                var pattern = '(' + utils.escapeRegExChars(currentValue) + ')';

                return suggestion.value
                    .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/"/g, '&quot;')
                    .replace(/&lt;(\/?strong)&gt;/g, '<$1>') + key;
            },
            onSelect: function(suggestion) {
                
            }
        });

        $('.autocomplete-specialty').devbridgeAutocomplete({
            serviceUrl: 'mocks/specialty.json',
            dataType: 'json',
            onSelect: function(suggestion) {
                
            }
        });

        $('.autocomplete-search').devbridgeAutocomplete({
            serviceUrl: 'mocks/doctors.json',
            dataType: 'json',
            formatResult: function(suggestion, currentValue) {
                var key = '<span class="suggestion-key">'+ suggestion.data +'</span>';
                if (!currentValue) {
                    return suggestion.value + key;
                }
                
                var pattern = '(' + utils.escapeRegExChars(currentValue) + ')';

                return suggestion.value
                    .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/"/g, '&quot;')
                    .replace(/&lt;(\/?strong)&gt;/g, '<$1>') + key;
            },
            onSelect: function(suggestion) {
                
            }
        });

        $('.box-mh').matchHeight({ property: 'min-height' });

        toogleDoctorInfoHandle();
    }
    
    return {
        init: init
    };

})(jQuery, window);

$(function(){
    app.init();
});