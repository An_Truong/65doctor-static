'use strict';

;(function ($, window, undefined) {
        var pluginName = 'maps';

        function Plugin(element, options) {
            this.element = $(element);
            this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
            this.init();
        }

        Plugin.prototype = {
            init: function() {
                this.id = this.element.attr('id');
                this.latLng = this.getLatlogByAddress(this.options.address);
            },

            getLatlogByAddress: function(address) {
                var geocoder = new google.maps.Geocoder(),
                    self = this;
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var data = {
                            lat: results[0].geometry.location.lat(),
                            lon: results[0].geometry.location.lng()
                        };
                        self.initMap(data, self.id);
                    } else {
                        
                    }
                });
            },

            initMap: function(data, id) {
                if(id) {
                    var latLng = new google.maps.LatLng(data.lat, data.lon),
                        mapOptions = {
                            zoom: 16,
                            center: latLng,
                            scrollwheel: false
                        },
                        map = new google.maps.Map(document.getElementById(id), mapOptions);

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map
                    });
                }
            }
        }

        $.fn[pluginName] = function(options) {
            var params = [].slice.call(arguments, 1);
            return this.each(function() {
                var instance = $.data(this, pluginName);
                if (!instance) {
                    $.data(this, pluginName, new Plugin(this, options));
                } else if (instance[options]) {
                    instance[options].apply(instance, params);
                }
            });
        };

        $.fn[pluginName].defaults = {};

        $(function() {
            $('[data-' + pluginName + ']')[pluginName]({});
        });

})(jQuery, window);