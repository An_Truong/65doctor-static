'use strict';

;(function ($, window, undefined) {
        var pluginName = 'autocomplete';

        function Plugin(element, options) {
            this.element = $(element);
            this.options = $.extend({}, $.fn[pluginName].defaults, this.element.data(), options);
            this.init();
        }

        Plugin.prototype = {
            init: function() {
                var self = this;
                this.element.devbridgeAutocomplete({
                    serviceUrl: this.options.url,
                    dataType: 'json',
                    formatResult: self.formatResult,
                    onSelect: function(suggestion) {
                        
                    }
                });
            },

            formatResult: function(suggestion, currentValue) {
                var key = '<span class="suggestion-key">'+ suggestion.data +'</span>';
                if (!currentValue) {
                    return suggestion.value + key;
                }
                
                var pattern = '(' + utils.escapeRegExChars(currentValue) + ')';

                return suggestion.value
                    .replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
                    .replace(/&/g, '&amp;')
                    .replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/"/g, '&quot;')
                    .replace(/&lt;(\/?strong)&gt;/g, '<$1>') + key;
            },

            initMap: function(data, id) {

            }
        }

        $.fn[pluginName] = function(options) {
            var params = [].slice.call(arguments, 1);
            return this.each(function() {
                var instance = $.data(this, pluginName);
                if (!instance) {
                    $.data(this, pluginName, new Plugin(this, options));
                } else if (instance[options]) {
                    instance[options].apply(instance, params);
                }
            });
        };

        $.fn[pluginName].defaults = {};

        $(function() {
            $('[data-' + pluginName + ']')[pluginName]({});
        });

})(jQuery, window);