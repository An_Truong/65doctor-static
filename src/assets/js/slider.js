'use strict';

;(function ($, window, undefined) {
    var slider = document.getElementById('slider'),
        leftValue = $('#leftvalue'),
        rightValue = $('#rightvalue');

    // 0 = initial minutes from start of day
    // 1410 = maximum minutes in a day (-30 minutes)
    // step: 30 = amount of minutes to step by. 
    var initialStartMinute = 0,
        initialEndMinute = 1410,
        step = 30;


    if(slider) {
        slider = noUiSlider.create(slider,{
            start:[initialStartMinute,initialEndMinute],
            connect:true,
            step:step,
            range:{
                'min':initialStartMinute,
                'max':initialEndMinute
            }
        });

        var convertValuesToTime = function(values,handle) {
            var hours = 0,
                minutes = 0;
              
            if(handle === 0) {
                hours = convertToHour(values[0]);
                minutes = convertToMinute(values[0],hours);
                leftValue.text(formatHoursAndMinutes(hours,minutes));
                return;
            }
          
            hours = convertToHour(values[1]);
            minutes = convertToMinute(values[1],hours);
            rightValue.text(formatHoursAndMinutes(hours,minutes));
            
        };

        var convertToHour = function(value) {
            return Math.floor(value / 60);
        };

        var convertToMinute = function(value,hour) {
            return value - hour * 60;
        };

        var formatHoursAndMinutes = function(hours,minutes) {
            var label = 'AM';
            if(hours.toString().length === 1) {
                hours = '0' + hours;
            } else {
                if(hours >= 12) {
                    label = 'PM';
                    if(hours > 12) {
                        hours =  hours - 12;
                    }
                }
            }
            if(minutes.toString().length === 1) {
                minutes = '0' + minutes;
            }
            return hours+':'+minutes + label;
        };

        slider.on('update',function(values,handle){
            convertValuesToTime(values,handle);
        });
    }

    $('.days .day').click(function() {
        $('.days .day').removeClass('active');
        $(this).addClass('active');
    });

})(jQuery, window);


